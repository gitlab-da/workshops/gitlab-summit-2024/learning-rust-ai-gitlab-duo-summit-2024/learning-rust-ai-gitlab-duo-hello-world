fn main() {
    println!("Hello, world!");

    // Print Hello GitLab Summit
    println!("Hello, GitLab Summit!");

    // Create a variable called name
    let name = "Michael";

    // Print Hello name
    println!("Hello, {}!", name);

    // Add two integer values

// Create two more integer variables x and y
let x = 5;
let y = 10;

// Add x and y and print result  
let sum = x + y;
println!("Sum of x and y is {}", sum);

// Subtract x and y and print result
let difference = x - y;
println!("Difference of x and y is {}", difference);

// Multiply x and y and print result
let product = x * y;
println!("Product of x and y is {}", product);

// Divide x and y and print result
let quotient = x / y;
println!("Quotient of x and y is {}", quotient);

// Modulo x and y and print result
let remainder = x % y;
println!("Remainder of x and y is {}", remainder);

// Create a variable called is_greater and assign it the result of x > y
let is_greater = x > y;

// Print the value of is_greater
println!("Is x greater than y? {}", is_greater);

// Create a variable called is_less and assign it the result of x < y

let is_less = x < y;

// Print the value of is_less
println!("Is x less than y? {}", is_less);

// Create a variable called is_equal and assign it the result of x == y
let is_equal = x == y;

// Print the value of is_equal
println!("Is x equal to y? {}", is_equal);

// Create a variable called is_not_equal and assign it the result of x != y
let is_not_equal = x != y;

// Print the value of is_not_equal
println!("Is x not equal to y? {}", is_not_equal);

// Flow control

// Create an if statement that checks if x is greater than y
if x > y {
    // Print "x is greater than y"
    println!("x is greater than y"); 
} else {
    // Print "x is less than or equal to y" 
    println!("x is less than or equal to y");
}

// Create a loop that prints the numbers from 1 to 10
for i in 1..11 {
    println!("Number: {}", i);
}

// Create a while loop that prints "Hello" 5 times
let mut counter = 0;
while counter < 5 {
    println!("Hello");
    counter += 1; 
}

// Create a for loop that prints the numbers from 1 to 10
for i in 1..11 {
    println!("Number: {}", i);
}

// Create a for loop that prints equal numbers in the range of 20

for i in 20..30 {
    if i % 2 == 0 {
        println!("{}", i); 
    }
}

// Create a conditional check whether a string contains the words "Everyone can contribute"

let message = "Everyone can contribute";
let contains_message = message.contains("Everyone can contribute");

if contains_message {
    println!("The message contains the words 'Everyone can contribute'");
} else {
    println!("The message does not contain the words 'Everyone can contribute'");
}

// Print the dates for the third Tuesday every month in 2024

use chrono::{Datelike, Weekday};

for month in 1..13 {
    let mut date = chrono::NaiveDate::from_ymd(2024, month, 1);

    while date.month() == month {
        if date.weekday() == Weekday::Tue && date.day() >= 15 && date.day() <= 21 {
            println!("{}", date);
        }

        date = date.succ(); 
    }
}

// Calculate a log rotation window based on crontab syntax
// For example, 5 * * * * as input

let cron = "5 * * * *";
let parts: Vec<&str> = cron.split(' ').collect();
let minute = parts[0]; 

let rotation = minute.parse::<u32>().unwrap();
println!("Log rotation window: Every {} minutes", rotation);

// Create a list with the GitLab Values: Collaboration, Results for Customers, Efficiency, Diversity, Inclusion and Belonging, Iteration, Transparency

let values = vec!["Collaboration", "Results for Customers", "Efficiency", "Diversity, Inclusion and Belonging", "Iteration", "Transparency"];

for value in &values {
    println!("{}", value);
}

// Create a hashmap of the list, and add a creative description

let mut values_map = std::collections::HashMap::new();
values_map.insert("Collaboration", "Working together as a team towards a common goal");  
values_map.insert("Results for Customers", "Providing value to customers through our products and services");
values_map.insert("Efficiency", "Continuously improving our processes to achieve more with less effort");
values_map.insert("Diversity, Inclusion and Belonging", "Fostering an environment where people of all backgrounds feel welcomed and able to contribute their perspectives"); 
values_map.insert("Iteration", "Rapidly delivering incremental improvements to quickly adapt based on feedback");
values_map.insert("Transparency", "Openly sharing information to build trust");

for (key, value) in &values_map {
    println!("{}: {}", key, value);
}


// Create a list of integers in the range of 50
// Check and print all prime numbers

let nums: Vec<u32> = (1..50).collect();

for num in &nums {
    if is_prime(*num) {
        println!("{} is prime", num);
    } 
}




}


// Create a function to check whether a number is a prime

fn is_prime(num: u32) -> bool {
    if num <= 1 {
        return false;
    }
    for i in 2..num {
        if num % i == 0 {
            return false; 
        }
    }
    true
}

mod tests {
    use super::*;
    #[test]
    fn test_is_prime() {
        assert_eq!(is_prime(1), false);
        assert_eq!(is_prime(2), true);
        assert_eq!(is_prime(3), true);
        assert_eq!(is_prime(4), false);
        assert_eq!(is_prime(5), true);
        assert_eq!(is_prime(6), false);
        assert_eq!(is_prime(7), true);
        assert_eq!(is_prime(8), false);
        assert_eq!(is_prime(9), false);
        assert_eq!(is_prime(10), false);
        assert_eq!(is_prime(11), true);
        assert_eq!(is_prime(12), true);
        assert_eq!(is_prime(13), true);
    }
}
